<?php

// blogRight! version 2.2
// BEGIN CONFIG

$CONF['url.site']         = 'http://your.site.name/blog';
$CONF['path.data']        = '/filesystem/path/blog/data/';
$CONF['path.header_file'] = 'templates/header.html';
$CONF['path.footer_file'] = 'templates/footer.html';
$CONF['charsetEncoding']  = 'UTF-8';
$CONF['recent']           = 'month';  // number of entries shown by default
$CONF['sortby']           = 'name';   // sort by: 'name', 'timestamp'
$CONF['sortorder']        = 'desc';   // sorting order: 'asc', 'desc'
$CONF['orderfile']        = '.order'; // an optional order file name
$CONF['permalinkFmt']     = 'Y/m/d';
$CONF['qpattern']         = ''; // empty for PrettyURI, otherwise '?q='

// Webxiangpianbu
$CONF['defaultGallery']   = 'photoblog';
$CONF['thumbnailPrefix']  = array ('default'   => 'small/small-',
				   'photoblog' => 'photoblog/small/small-');

$CONF['patternURL.img']     = '<img src="http://your.site.name/images/%IMAGE" alt="[image]"%PARAMS />';
$CONF['patternURL.link']    = '<a href="http://your.site.name/%URI">';
$CONF['patternURL.photo']   = '<a href="photos/%PHOTO"><img src="http://your.site.name/photos/data/%THUMBNAIL" alt="[photo]"%PARAMS /></a>';
$CONF['patternURL.photos']  = '<a href="photos/%ALBUM/%PHOTO"><img src="http://your.site.name/photos/data/%THUMBNAIL" alt="[photo]"%PARAMS /></a>';
$CONF['patternURL.gallery'] = '<a href="photos/%ALBUM/"><img src="http://your.site.name/photos/data/%THUMBNAIL" alt="[gallery]"%PARAMS /><br />&#187;</a>';

// RSS/Atom
$CONF['feed.title']       = 'title';
$CONF['feed.description'] = "description";
$CONF['feed.copyright']   = 'Copyright (C) Your Name';
$CONF['feed.authorName']  = 'Your Name';
$CONF['feed.authorEmail'] = 'Your e-mail address';
$CONF['feed.editor']      = "Your e-mail address (Your Name)";
$CONF['feed.tagUri']      = 'tag:EMAIL,YEAR:NAME';
$CONF['feed.language']    = 'en'; // Two-letter language code as per ISO 639
$CONF['feed.recent']      = '10';
$CONF['feed.XHTML']       = true;
$CONF['feed.CDATA']       = true;

// Calendar
$CONF['calendar.emptyMonths'] = false;
$CONF['calendar.vertical']    = false;

// END CONFIG

?>
